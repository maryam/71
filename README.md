# Flask Secure application

This is a template for a Flask application, using Flask-Security-Too and SQLAlchemy.

## Usage

1. Clone the project
2. Rename prod.env or dev.env to .env
3. In .env, modify what you need to
4. Create and activate python virtual env
5. Install requirements with `pip install -r requirements.txt`
6. Generate secret keys and paswords salts and put them in .env : `openssl rand -hex 32`
6. Now you can run your app with `flask run`